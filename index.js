var delayPost = 3000;
var port = 3000;
var token = "123456789";
var password = 'olivier';

var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);

app.use(function (req, res, next) {
  console.log('-> Request : ' + req.url);

  return next();
});

app.get('/', function(req, res, next){
  res.sendFile('index.html',  { root: __dirname });
});

app.get('/chat-frontend.js', function(req, res, next){
  res.sendFile('chat-frontend.js',  { root: __dirname });
});

app.get('/login', function(req, res, next){

  var userPassword = req.query.password;
  console.log('password : ' + password);
  if (password == userPassword) {
      res.send('{"success":true, "token":'+token+'}');
  } else {
      res.status(401).send('{"success":false}');
  }

});

app.ws('/', function(ws, req) {
  ws.on('connection', function (ws) {
      console.log('socket] connection...');
  })

  ws.on('message', function(msg) {
    console.log('[socket] ' + msg);
    broadcast(msg, 'user');
  });

  console.log('[socket] init');
  if (req.query.token == '' || req.query.token != token) {
      console.log('[socket] close');
      req.close();
  } else {
      console.log('[socket] go');
  }
});

setInterval(function() {
    if (expressWs.getWss().clients) {
        console.log('[socket] Sending test...');
        broadcast('test ' + (new Date()).getTime(), 'Websocket server');
    }
}, delayPost);

function broadcast(message, author) {
     var obj = {
                    time: (new Date()).getTime(),
                    text: message,
                    author: author,
                };
    var json = JSON.stringify({ type:'message', data: obj });
    for (var i=0; i < expressWs.getWss().clients.length; i++) {
    console.log('[socket] Sending message to client ' + i + ' : ' + json);
        expressWs.getWss().clients[i].send(json);
    } 
}

app.listen(port);